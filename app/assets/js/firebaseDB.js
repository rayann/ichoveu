function buscarPlaca() {
  const placa = $("#placa").val();

  $.ajax({
    method: 'GET',
    url: '/busca/' + placa,
    success: function(response) {
      if (response.placaEnc !== undefined) {
        $('#placaEnc').text(response.placaEnc);
        $('#end').text(response.endEnc);
        $('#nome').text(response.nomeEnc);
        $('#tel').text(response.telEnc);
        $('#detail').show();
        $('#msg').hide();
      } else {
        $('#detail').hide();
        $('#msg').show();
      }
    },
    error: function(err) {
      console.log(err);
    }
  });
}
