'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const firebase = require("firebase");
const path = require('path');

// Configuração do firebase
firebase.initializeApp({
  apiKey: "AIzaSyB6bnoGPixf1ZreOTutgHLNnIPSI1M0ZHE",
  authDomain: "ichoveu.firebaseapp.com",
  databaseURL: "https://ichoveu.firebaseio.com",
  storageBucket: "ichoveu.appspot.com",
});

// configuração para receber dados da requisição
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// mapeando pasta para arquivos estáticos
app.use(express.static(__dirname + '/app'));
app.set('views', __dirname + '/app');
app.set('view engine', 'ejs');


// rota para salvar placa encontrada
app.post('/achei', (req, res) => {
  firebase.database().ref('/'+req.body.placaEnc).set(req.body);
  res.sendFile(path.join(__dirname+'/app/index.html'));
});

// rota para salvar placa voluntario
app.post('/voluntario', (req, res) => {
  firebase.database().ref('/'+req.body.cpf).set(req.body);
  res.sendFile(path.join(__dirname+'/app/index.html'));
});


//rota para busca
app.get('/busca/:placa', (req, res) => {
  firebase.database().ref().child("/"+req.params.placa).on("value", (snapshot) => {
    res.send(snapshot.val());
  });
})

// porta que a aplicação vai escultar
app.listen(3000);
